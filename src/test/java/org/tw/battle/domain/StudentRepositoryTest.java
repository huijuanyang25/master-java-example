package org.tw.battle.domain;

import org.jooq.Record1;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.Test;
import org.tw.battle.dbTest.InMemoryDbSupport;
import org.tw.battle.dbTest.TestDatabaseConfiguration;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import static org.junit.jupiter.api.Assertions.*;

@InMemoryDbSupport
class StudentRepositoryTest {
    @Test
    void should_create_student() throws Exception {
        final ServiceConfiguration configuration = TestDatabaseConfiguration.getConfiguration();

        final StudentRepository repository = new StudentRepository(configuration);
        final int id = repository.create("Wang Jingze");

        // I use an ORM because I don't want to illustrate how to use JDBC to
        // execute query :-D
        final Record1<String> record = DSL.using(DatabaseConnectionProvider.createConnection(configuration), SQLDialect.H2)
            .select(DSL.field("name", String.class))
            .from(DSL.table("student"))
            .where(DSL.field("id").eq(id))
            .fetchOne();

        assertEquals("Wang Jingze", record.get("name", String.class));
    }
}